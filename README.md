# What it is?
PayU.Sharp is an PayU API integration with .NET Core 2.1+
It's based on PayU documentation [http://developers.payu.com/en/restapi.html](http://developers.payu.com/en/restapi.html)

# HowTo

Before you start, you will need PayU account with a shop that has REST-API endpoint enabled. To do this follow PayU tutorials.
Or go through those steps:
1. Login into your PayU account (either sandbox or normal one)
2. Go to "Online payments"
3. Follow up to "My shops"
4. Click "Add shop" on right side of screen
5. Now follow up the creator
    1. In POS type select REST API (Checkout)
    2. Give it any name you wish, we won't use that.
6. Confirm and download data which will appear on screen containing POS ID, CLIENT ID and CLIENT SECRET.
Now you have all data needed by our API.

# Methods and API set-up

First, before using any method in this API package you need to authorize via OAUTH to PayU servers.  
You can use either static access via PayU.API 
or create an instance of PayU.APIClient and use it to operate via PayU REST API.

```cs
OAuthResponse PayU.API.Authorize(int clientId, string clientSecret)
OAuthResponse PayU.API.Authorize(int clientId, string clientSecret, string grantType, string email, string extCustomerId)
```
This method consumes CLIENT ID and CLIENT SECRET you've got while creating shop. It returns OAuthResponse, which contains all data returned by PayU servers, however API is automated and saves that data into object, which is automatically recognized and processed by next methods.

Version with email, grantType and extCustomerId is commonly used when downloading payment infromation for specified user. To check it refer to PayU docs.

Now, as you're authorized you can do one of the following:

```cs
OrderCreateResponse PayU.API.CreateOrder(OrderCreateRequest request)
```
Creates new order in PayU system. OrderCreateRequest object structure is available in PayU REST API documentation. (see links)

```cs
RefundCreateResponse PayU.API.CreateRefund(RefundCreateRequest request)
```
Creates new refund in PayU system. Remember that some params must be correct for refund to be processed.

```cs
OrderCancelResponse PayU.API.CancelOrder(OrderCancelRequest request)
```
Cancels order. Only applies to order that has not yet been COMPLETEd.

```cs
OrderStatusUpdateResponse PayU.API.UpdateOrderStatus(OrderStatusUpdateRequest request)
```
Updates order status. Only applies to orders that has not yet been COMPLETEd, but has been paid.

```cs
OrderRetrieveResponse PayU.API.GetOrderData(OrderRetrieveRequest request)
```
Gets Order data from PayU. Beware that Buyer might be null before the offer has been COMPLETEd.

```cs
TransactionRetrieveResponse PayU.API.GetTransactionData(TransactionRetrieveRequest request)
```
Returns data about transaction incl. bank account data and credit card data with masked number.

```cs
PayMethodsResponse PayU.API.GetPaymentMethods(string lang = "en")
```
Gets available payment methods. By default all descriptions are in "EN". However "DE", "PL" and "CS" are fully supported too. Support for languages is provided by PayU itself.

```cs
int PayU.API.DeleteToken(string tkn)
```
Deletes OAuth token from PayU servers. Returns 204 or Error Code.


# Additions
1. You've also `StatusCodes` class for your usage, when it comes to parsing errors received from PayU systems.
2. `APIClient` contains also fields to verify if authorization is still valid - `AuthorizationValidityTime`, `LastAuthorization` and `IsAuthorized`
3. By default API uses Sandbox. You can change that by changing the `UseSandbox` field value in `APIClient`


# Links
1. [PayU](https://payu.com)
2. [PayU REST API Docs](http://developers.payu.com/en/restapi.html)
3. [PayU APIary Docs](https://payu21.docs.apiary.io/)

# To Be Implemented
1. Payout Endpoint

# Authors
Authors are not related to PayU S.A. and this software has been developed without any instruction or payment from PayU S.A.  
Software has been developed to make developing ASP.NET Core services easier.  
Software uses PayU name as reference to describe features related to third party service the software uses to function.  
If any proper owner of names used in this package wants for it to be removed feel free to contact with us at copyright@itnnovative.com.  

Implementation is provided by <br/>
<a href="https://itnnovative.com"><img src="https://itnnovative.com/assets/logo/black.png" width="300" height="65"/></a>

