using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PayU.Internal;
using PayU.Models;

namespace PayU
{
    // ReSharper disable once InconsistentNaming
    public class APIClient
    {
        #region SERVER_DATA
        /// <summary>
        /// Primary Server URI [Production]
        /// </summary>
        public const string SERVER_URI = "https://secure.payu.com";
        
        /// <summary>
        /// Sandbox Server URI [Testing]
        /// </summary>
        public const string SANDBOX_URI = "https://secure.snd.payu.com";
        #endregion
        
        #region ENDPOINTS
        public const string AUTH_URI = "/pl/standard/user/oauth/authorize";

        public const string CREATE_ORDER_ENDPOINT = "/api/v2_1/orders";
        public const string CREATE_REFUND_ENDPOINT = "/api/v2_1/orders/{orderId}/refunds";
        public const string CANCEL_ORDER_ENDPOINT = "/api/v2_1/orders/{orderId}";
        public const string UPDATE_STATUS_ENDPOINT = "/api/v2_1/orders/{orderId}/status";
        public const string ORDER_GET = "/api/v2_1/orders/{orderId}";
        public const string TRANSACTION_GET_STATUS = "/api/v2_1/orders/{orderId}/transactions";
        public const string PAYMENT_METHODS = "/api/v2_1/paymethods";
        public const string DELETE_TOKEN_ENDPOINT = "/api/v2_1/tokens/";
        public const string CREATE_PAYOUT_ENDPOINT = "/api/v2_1/payouts";
        public const string GET_PAYOUT_ENDPOINT = "/api/v2_1/payouts/{id}";
        #endregion

        /// <summary>
        /// Last-known authorization information. Useful when it comes to sending requests to PayU.
        /// </summary>
        public OAuthResponse Authorization;

        /// <summary>
        /// Date of Last known authorization
        /// </summary>
        public DateTime LastAuthorization = DateTime.MinValue;

        /// <summary>
        /// Time to which the Authorization is valid
        /// </summary>
        public DateTime AuthorizationValidityTime => LastAuthorization.AddSeconds(Authorization.ExpiresIn - 10);
        
        /// <summary>
        /// Set to true if it should use sandbox servers, to false if you want to build production version.
        /// </summary>
        public bool UseSandbox = true;

        /// <summary>
        /// Returns PayU Server URI for usage. Picks either Sandbox or Primary URI basing on your settings.
        /// </summary>
        /// <returns>PayU Server URI which API will use to communicate with PayU systems.</returns>
        public string ServerUri => UseSandbox ? SANDBOX_URI : SERVER_URI;

        /// <summary>
        /// Checks if Authorization is still valid. If yes, then returns true, false otherwise.
        /// If it's false then you should Authorize() again
        /// </summary>
        public bool IsAuthorized => AuthorizationValidityTime > DateTime.UtcNow;
        
        /// <summary>
        /// Authorizes in PayU system.
        /// </summary>
        /// <param name="clientId">Client ID to be used as authorization</param>
        /// <param name="clientSecret">Client Secret</param>
        /// <param name="grantType">Grant Type (default "client_credentials"), may be also "trusted_merchant"</param>
        /// <returns>Authorization response, which is also set as API current key.</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<OAuthResponse> Authorize(int clientId, string clientSecret, string grantType = "client_credentials", string email = "", string extCustomerId = "")
        {
            // Create new HTTP Client
            var wcl = new HttpClient();
            
            // Create Authorization URI
            var uri = ServerUri + AUTH_URI + "?grant_type=" + grantType + "&client_id=" + clientId +
                      "&client_secret=" + clientSecret;
            if (grantType == "trusted_merchant")
            {
                uri += "&ext_customer_id=" + extCustomerId;
                uri += "&email=" + email;
            }
    
            var http = (HttpWebRequest) WebRequest.Create(new Uri(uri));
            
            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";
            http.AllowAutoRedirect = false;

            // Build body
            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;

            // Send request to PayU
            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OAuthResponse>(rData);
            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OAuthResponse>(rData);
            }
            
            // Send request and parse data
            var text = await wcl.GetStringAsync(uri);
            var auth = JsonConvert.DeserializeObject<OAuthResponse>(text);
    
            Authorization = auth;
            LastAuthorization = DateTime.UtcNow;
            return auth;
        }     

        /// <summary>
        /// Creates refund
        /// </summary>
        /// <returns><see cref="RefundCreateResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<RefundCreateResponse> CreateRefund(RefundCreateRequest request)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri + CREATE_REFUND_ENDPOINT.Replace("{orderId}", request.OrderId)));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.MetadataPropertyHandling = MetadataPropertyHandling.Ignore;
            var parsedContent = JsonConvert.SerializeObject(request, settings);

            // Convert and send as json
            var encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(parsedContent);

            // Send request to PayU
            try
            {
                var newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<RefundCreateResponse>(rData);

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<RefundCreateResponse>(rData);
            }
        }
        
        /// <summary>
        /// Creates order
        /// </summary>
        /// <returns><see cref="OrderCreateResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<OrderCreateResponse> CreateOrder(OrderCreateRequest request)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri + CREATE_ORDER_ENDPOINT));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            var parsedContent = JsonConvert.SerializeObject(request, settings);

            // Convert and send as json
            var encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(parsedContent);

            // Send request to PayU
            try
            {
                var newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<OrderCreateResponse>(rData);

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OrderCreateResponse>(rData);
            }
        }
        
        /// <summary>
        /// Retrieves payout from server
        /// </summary>
        /// <returns><see cref="PayoutRetrieveResponse"/> with server-returned data</returns>
        public async Task<PayoutRetrieveResponse> RetrievePayout(PayoutRetrieveRequest request)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri + CREATE_PAYOUT_ENDPOINT));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            
            // Send request to PayU
            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<PayoutRetrieveResponse>(rData);

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<PayoutRetrieveResponse>(rData);
            }
        }
        
        /// <summary>
        /// Creates new payout
        /// </summary>
        /// <returns><see cref="PayoutResponse"/> with server-returned data</returns>
        public async Task<PayoutResponse> CreatePayout(PayoutRequest request)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri + CREATE_PAYOUT_ENDPOINT));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            var parsedContent = JsonConvert.SerializeObject(request, settings);

            // Convert and send as json
            var encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(parsedContent);

            // Send request to PayU
            try
            {
                var newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<PayoutResponse>(rData);

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<PayoutResponse>(rData);
            }
        }
        
        /// <summary>
        /// Cancels order if possible
        /// </summary>
        /// <returns><see cref="OrderCancelResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<OrderCancelResponse> CancelOrder(OrderCancelRequest request)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri + CANCEL_ORDER_ENDPOINT.Replace("{orderId}", request.OrderId)));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "DELETE";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            var parsedContent = JsonConvert.SerializeObject(request, settings);

            // Convert and send as json
            var encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(parsedContent);

            // Send request to PayU
            try
            {
                
                var newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                var obj = JsonConvert.DeserializeObject<OrderCancelResponse>(rData);
                return obj;

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OrderCancelResponse>(rData);
            }
        }
        
        /// <summary>
        /// Updates order status
        /// </summary>
        /// <returns><see cref="OrderStatusUpdateResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public  async Task<OrderStatusUpdateResponse> UpdateOrderStatus(OrderStatusUpdateRequest request)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri +  UPDATE_STATUS_ENDPOINT.Replace("{orderId}", request.OrderId)));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "PUT";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            var parsedContent = JsonConvert.SerializeObject(request, settings);

            // Convert and send as json
            var encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(parsedContent);

            // Send request to PayU
            try
            {
                var newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<OrderStatusUpdateResponse>(rData);

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OrderStatusUpdateResponse>(rData);
            }
        }
        
        /// <summary>
        /// Gets Order Data
        /// </summary>
        /// <returns><see cref="OrderRetrieveResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<OrderRetrieveResponse> GetOrderData(OrderRetrieveRequest request)
        {
            var token = Authorization.AccessToken;

            var uri = ServerUri + ORDER_GET.Replace("{orderId}", request.OrderId);
            var http = (HttpWebRequest) WebRequest.Create(new Uri(uri));
            
            
            
            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body
            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;

            // Send request to PayU
            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OrderRetrieveResponse>(rData);
            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<OrderRetrieveResponse>(rData);
            }
        }
     
        /// <summary>
        /// Gets Transaction Data
        /// </summary>
        /// <returns><see cref="TransactionRetrieveResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<TransactionRetrieveResponse> GetTransactionData(TransactionRetrieveRequest request)
        {
            var token = Authorization.AccessToken;

            var uri = ServerUri + TRANSACTION_GET_STATUS.Replace("{orderId}", request.OrderId);
            var http = (HttpWebRequest) WebRequest.Create(new Uri(uri));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;

            // Send request to PayU
            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<TransactionRetrieveResponse>(rData);

            }
            catch (WebException e)
            {
                 var response = e.Response;
                 var stream = response.GetResponseStream();
                 var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                 var rData = await sr.ReadToEndAsync();
                 return JsonConvert.DeserializeObject<TransactionRetrieveResponse>(rData);
            }
        }
        
     
        /// <summary>
        /// Downloads available payment methods
        /// </summary>
        /// <returns><see cref="PayMethodsResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public async Task<PayMethodsResponse> GetPaymentMethods(string lang = "en")
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri +  PAYMENT_METHODS + "?lang=" + lang));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;

            // Send request to PayU
            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<PayMethodsResponse>(rData);

            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<PayMethodsResponse>(rData);
            }
        }
        
        /// <summary>
        /// Deletes token if possible
        /// </summary>
        /// <returns>Status of operation, 204 for success</returns>
        public async Task<int> DeleteToken(string tkn)
        {
            var token = Authorization.AccessToken;

            var http = (HttpWebRequest) WebRequest.Create(new Uri(ServerUri + DELETE_TOKEN_ENDPOINT + tkn));

            // Create headers
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "DELETE";
            http.AllowAutoRedirect = false;

            http.Headers.Add("Authorization", "Bearer " + token);

            // Build body

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;


            // Send request to PayU
            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return 204;
            }
            catch (WebException e)
            {
                var response = e.Response;
                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream ?? throw new InvalidOperationException());
                var rData = await sr.ReadToEndAsync();

                return (int)((HttpWebResponse) e.Response).StatusCode;
            }
        }
    }
}