using System;

namespace PayU.Internal
{
    /// <summary>
    /// Used by ValidatableObject to check if object is proper
    /// </summary>
    public class RequiredAttribute : Attribute
    {
        
    }
}