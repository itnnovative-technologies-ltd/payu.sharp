using System;

namespace PayU.Internal.Errors
{
    /// <summary>
    /// Represents error of 
    /// </summary>
    public class ValidationFailedException : Exception
    {
        public string Field;
        
        public ValidationFailedException(string field)
        {
            Field = field;
        }
        
        public new string Message => $"Validation failed. You've not set an required field { Field }";
    }
}