using System;

namespace PayU.Internal
{
    /// <summary>
    /// Represents confirmation of method verification. It means that method has been checked on Sandbox server.
    /// </summary>
    public class VerifiedAttribute : Attribute
    {
        public string User;
        
        public VerifiedAttribute(string user)
        {
            User = user;
        }
    }
}