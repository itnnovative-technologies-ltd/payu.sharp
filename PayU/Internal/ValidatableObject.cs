using System;
using System.Linq;
using System.Reflection;
using PayU.Internal.Errors;

namespace PayU.Internal
{
    /// <summary>
    /// Validatable Object represents object that can be checked for validity
    /// </summary>
    public abstract class ValidatableObject
    {
        /// <summary>
        /// Validates this object
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            var fieldInfos = GetType().GetFields(BindingFlags.NonPublic |
                                                 BindingFlags.Instance | BindingFlags.Public);
            
            // Validate all required fields
            foreach (var v in fieldInfos)
            {
                // Get type and value
                var type = v.GetType();
                var val = v.GetValue(this);
                
                // Get attributes
                if (v.GetCustomAttributes<RequiredAttribute>().ToList().Count > 0)
                {
                    // If value is null, then validation failed
                    if (val == null)
                    {
                        throw new ValidationFailedException(v.Name);
                    }
                    
                    if (type == typeof(string))
                    {
                        // If string is not set, then validation failed
                        if (string.IsNullOrEmpty((string) val))
                        {
                            throw new ValidationFailedException(v.Name);
                        }
                    }
                    else if (val is ValidatableObject)
                    {
                        // If object does not validate, the it's failed
                        if (!(val as ValidatableObject).Validate())
                        {
                            throw new ValidationFailedException(v.Name);
                        }
                    }
                }
            }

            return true;
        }
    }
}