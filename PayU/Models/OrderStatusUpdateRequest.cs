using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents order sent to update status
    /// </summary>
    [Serializable]
    public class OrderStatusUpdateRequest : ValidatableObject
    {
        [Required] [JsonProperty] internal string orderId;
        [Required] [JsonProperty] internal string orderStatus;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }

        /// <summary>
        /// New Status ex. COMPLETED
        /// </summary>
        [JsonIgnore] public string OrderStatus
        {
            get => orderStatus;
            set => orderStatus = value;
        }
    }
}