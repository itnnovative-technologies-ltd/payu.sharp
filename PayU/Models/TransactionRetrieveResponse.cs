using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PayU.Models
{
    /// <summary>
    /// Received on transactions download request
    /// </summary>
    public class TransactionRetrieveResponse
    {
        [JsonProperty] internal List<Transaction> transactions;

        [JsonIgnore] public List<Transaction> Transactions
        {
            get => transactions;
            set => transactions = value;
        }
    }
}