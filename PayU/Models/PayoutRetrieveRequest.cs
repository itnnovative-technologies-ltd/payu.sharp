using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Request to download payout data
    /// </summary>
    public class PayoutRetrieveRequest
    {
        [JsonProperty] internal string payoutId;

        /// <summary>
        /// ID of Payout
        /// </summary>
        public string PayoutId
        {
            get => payoutId;
            set => payoutId = value;
        }
    }
}