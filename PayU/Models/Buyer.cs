using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents customer. Including address, names, and electronic data.
    /// </summary>
    [Serializable]
    public class Buyer : ValidatableObject
    {
        [JsonProperty] internal string customerId;
        [JsonProperty] internal string extCustomerId;
        [Required] [JsonProperty] internal string email;
        [JsonProperty] internal string phone;
        [JsonProperty] internal string firstName;
        [JsonProperty] internal string lastName;
        [JsonProperty] internal string nin;
        [JsonProperty] internal string language;
        [JsonProperty] internal Delivery delivery;

        /// <summary>
        /// PayU Customer ID
        /// </summary>
        [JsonIgnore] public string CustomerId
        {
            get => customerId;
            set => customerId = value;
        }

        /// <summary>
        /// Your Customer ID
        /// </summary>
        [JsonIgnore] public string ExtCustomerId
        {
            get => extCustomerId;
            set => extCustomerId = value;
        }

        /// <summary>
        /// Customer's email
        /// </summary>
        [JsonIgnore] public string Email
        {
            get => email;
            set => email = value;
        }

        /// <summary>
        /// Customer's phone
        /// </summary>
        [JsonIgnore] public string Phone
        {
            get => phone;
            set => phone = value;
        }

        /// <summary>
        /// Name of customer
        /// </summary>
        [JsonIgnore] public string FirstName
        {
            get => firstName;
            set => firstName = value;
        }

        /// <summary>
        /// Surname / Last Name of customer
        /// </summary>
        [JsonIgnore] public string LastName
        {
            get => lastName;
            set => lastName = value;
        }

        /// <summary>
        /// National Insurance Number
        /// for UK: NIN
        /// for Poland: PESEL
        /// etc.
        /// </summary>
        [JsonIgnore] public string Nin
        {
            get => nin;
            set => nin = value;
        }

        /// <summary>
        /// Language to use
        /// Available: pl, en, de, cs
        /// More also available, but not fully supported.
        /// <see cref="http://developers.payu.com/en/overview.html#languages"/>
        /// </summary>
        [JsonIgnore] public string Language
        {
            get => language;
            set => language = value;
        }

        /// <summary>
        /// Delivery location of package.
        /// </summary>
        [JsonIgnore] public Delivery Delivery
        {
            get => delivery;
            set => delivery = value;
        }
    }
}