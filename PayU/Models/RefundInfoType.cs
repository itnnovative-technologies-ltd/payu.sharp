using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents information about refunds
    /// </summary>
    [Serializable]
    public class RefundInfoType : ValidatableObject
    {
        [Required] [JsonProperty] internal string description;
        [JsonProperty] internal string amount;
        [JsonProperty] internal string extRefundId;
        [JsonProperty] internal string bankDescription;
        [JsonProperty] internal string type;

        /// <summary>
        /// Refund reason
        /// </summary>
        [JsonIgnore] public string Description
        {
            get => description;
            set => description = value;
        }

        /// <summary>
        /// Amount of money to be refunded
        /// </summary>
        [JsonIgnore] public int Amount
        {
            get => int.Parse(amount);
            set => amount = value.ToString();
        }

        /// <summary>
        /// Refund ID in your shop
        /// </summary>
        [JsonIgnore] public string ExtRefundId
        {
            get => extRefundId;
            set => extRefundId = value;
        }

        /// <summary>
        /// Name of bank reference
        /// </summary>
        [JsonIgnore] public string BankDescription
        {
            get => bankDescription;
            set => bankDescription = value;
        }

        /// <summary>
        /// Refund type,
        /// Possible: ex. REFUND_PAYMENT_STANDARD
        /// </summary>
        [JsonIgnore] public string Type
        {
            get => type;
            set => type = value;
        }
    }
}