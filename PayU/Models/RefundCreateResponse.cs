using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Received on refund request
    /// </summary>
    public class RefundCreateResponse
    {
        [JsonProperty] internal string orderId;
        [JsonProperty] internal RefundRecordType refund;
        [JsonProperty] internal Status status;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }

        /// <summary>
        /// Refund information
        /// </summary>
        [JsonIgnore] public RefundRecordType Refund
        {
            get => refund;
            set => refund = value;
        }

        /// <summary>
        /// Refund status information
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }

    }
}