using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents payout request
    /// </summary>
    public class PayoutRequest
    {
        [JsonProperty] internal string shopId;
        [JsonProperty] public PayoutRequestPayout payout;

        /// <summary>
        /// ID of shop to get paid from
        /// </summary>
        [JsonIgnore] public string ShopId
        {
            get => shopId;
            set => shopId = value;
        }

        /// <summary>
        /// Payout data
        /// </summary>
        [JsonIgnore] public PayoutRequestPayout Payout
        {
            get => payout;
            set => payout = value;
        }
    }
}