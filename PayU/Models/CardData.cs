using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents credit card data
    /// <see cref="http://developers.payu.com/en/restapi.html#references_card_statuses"/>
    /// </summary>
    [Serializable]
    public class CardData
    {
        [JsonProperty] internal string cardNumberMasked;
        [JsonProperty] internal string cardScheme;
        [JsonProperty] internal string cardProfile;
        [JsonProperty] internal string cardClassification;
        [JsonProperty] internal string cardResponseCode;
        [JsonProperty] internal string cardResponseCodeDesc;
        [JsonProperty] internal string cardEciCode;
        [JsonProperty] internal string card3DsStatus;
        [JsonProperty] internal string cardBinCountry;

        /// <summary>
        /// Card number
        /// </summary>
        [JsonIgnore] public string CardNumberMasked
        {
            get => cardNumberMasked;
            set => cardNumberMasked = value;
        }

        /// <summary>
        /// Card Scheme
        /// MC - MasterCard
        /// VS - Visa
        /// </summary>
        [JsonIgnore] public string CardScheme
        {
            get => cardScheme;
            set => cardScheme = value;
        }

        /// <summary>
        /// Card profile
        /// CONSUMER / BUSINESS
        /// </summary>
        [JsonIgnore] public string CardProfile
        {
            get => cardProfile;
            set => cardProfile = value;
        }

        /// <summary>
        /// Card classification
        /// DEBIT / CREDIT
        /// </summary>
        [JsonIgnore] public string CardClassification
        {
            get => cardClassification;
            set => cardClassification = value;
        }

        /// <summary>
        /// <see cref="http://developers.payu.com/en/restapi.html#references_card_statuses"/>
        /// </summary>
        [JsonIgnore] public string CardResponseCode
        {
            get => cardResponseCode;
            set => cardResponseCode = value;
        }

        /// <summary>
        /// <see cref="http://developers.payu.com/en/restapi.html#references_card_statuses"/>
        /// </summary>
        [JsonIgnore] public string CardResponseCodeDesc
        {
            get => cardResponseCodeDesc;
            set => cardResponseCodeDesc = value;
        }

        /// <summary>
        /// <see cref="http://developers.payu.com/en/restapi.html#references_card_statuses"/>
        /// </summary>
        [JsonIgnore] public string CardEciCode
        {
            get => cardEciCode;
            set => cardEciCode = value;
        }

        /// <summary>
        /// <see cref="http://developers.payu.com/en/restapi.html#references_card_statuses"/>
        /// </summary>
        [JsonIgnore] public string Card3DsStatus
        {
            get => card3DsStatus;
            set => card3DsStatus = value;
        }

        /// <summary>
        /// Country related to card
        /// </summary>
        [JsonIgnore] public string CardBinCountry
        {
            get => cardBinCountry;
            set => cardBinCountry = value;
        }

    }
}