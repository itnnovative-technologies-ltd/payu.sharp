using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents PayU OAuth Response.
    /// </summary>
    [Serializable]
    public class OAuthResponse
    {
        // JSON Response parameters
        [JsonProperty] internal string access_token;
        [JsonProperty] internal string token_type;
        [JsonProperty] internal string refresh_token;
        [JsonProperty] internal int expires_in;
        [JsonProperty] internal string grant_type;

        /// <summary>
        /// Access token to be used.
        /// </summary>
        [JsonIgnore] public string AccessToken
        {
            get => access_token;
            set => access_token = value;
        }

        /// <summary>
        /// Token type, usually Bearer
        /// </summary>
        [JsonIgnore] public string TokenType
        {
            get => token_type;
            set => token_type = value;
        }

        /// <summary>
        /// Time in seconds after which this token will expiry
        /// </summary>
        [JsonIgnore] public int ExpiresIn
        {
            get => expires_in;
            set => expires_in = value;
        }

        /// <summary>
        /// Token grant type, usually client_credentials
        /// </summary>
        [JsonIgnore] public string GrantType
        {
            get => grant_type;
            set => grant_type = value;
        }

        /// <summary>
        /// Token used to refresh
        /// </summary>
        [JsonIgnore] public string RefreshToken
        {
            get => refresh_token;
            set => refresh_token = value;
        }
    }
}