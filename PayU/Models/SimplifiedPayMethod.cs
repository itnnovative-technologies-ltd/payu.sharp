using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents simplified pay method information used in Transaction Information
    /// <see cref="Transaction"/>
    /// </summary>
    public class SimplifiedPayMethod
    {
        [JsonProperty] internal string value;

        /// <summary>
        /// Payment Method Type
        /// </summary>
        [JsonIgnore] public string Value
        {
            get => value;
            set => this.value = value;
        }
    }
}