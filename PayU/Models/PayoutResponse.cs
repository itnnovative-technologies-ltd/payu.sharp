using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PayU.Models
{
    /// <summary>
    /// Represents data returned by server on payout creation
    /// </summary>
    public class PayoutResponse
    {
        [JsonProperty] internal Payout payout;
        [JsonProperty] internal Status status;

        /// <summary>
        /// Payout created via request
        /// </summary>
        [JsonIgnore] public Payout Payout
        {
            get => payout;
            set => payout = value;
        }

        /// <summary>
        /// Status of payout
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }
    }
}