namespace PayU.Models
{
    public class CardOnFileC
    {
        /// <summary>
        /// Payment initiated by owner of the card.
        /// </summary>
        public const string STANDARD_CARDHOLDER = "STANDARD_CARDHOLDER";
        
        /// <summary>
        /// Payment initiated by shop (eg. automatic payment)
        /// </summary>
        public const string STANDARD_MERCHANT = "STANDARD_MERCHANT";
    }

    public enum CardOnFile
    {
        STANDARD_CARDHOLDER = 0,
        STANDARD_MERCHANT = 1
    }
}