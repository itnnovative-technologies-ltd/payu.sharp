using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents data received when downloading payment methods
    /// </summary>
    [Serializable]
    public class PayMethodsResponse
    {
        [JsonProperty] internal List<PayMethodType> payByLinks;
        [JsonProperty] internal List<PexToken> pexTokens;
        [JsonProperty] internal List<CardToken> cardTokens;
        
        /// <summary>
        /// Payment methods list
        /// </summary>
        [JsonIgnore] public List<PayMethodType> PayByLinks
        {
            get => payByLinks;
            set => payByLinks = value;
        }

        /// <summary>
        /// List of available saved cards
        /// </summary>
        [JsonIgnore] public List<CardToken> CardTokens
        {
            get => cardTokens;
            set => cardTokens = value;
        }
        
        
        /// <summary>
        /// List of available saved bank accounts
        /// </summary>
        [JsonIgnore] public List<PexToken> PexTokens
        {
            get => pexTokens;
            set => pexTokens = value;
        }
    }
}