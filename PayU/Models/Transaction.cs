using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents Transaction data incl. Bank Account or Card
    /// </summary>
    public class Transaction
    {
        [JsonProperty] internal SimplifiedPayMethod payMethod;
        [JsonProperty] internal string paymentFlow;
        [JsonProperty] internal Card card;
        [JsonProperty] internal BankAccount bankAccount;

        /// <summary>
        /// Bank account used in transaction, null if credit card was used
        /// </summary>
        [JsonIgnore] public BankAccount BankAccount
        {
            get => bankAccount;
            set => bankAccount = value;
        }
        
        /// <summary>
        /// Card used in transaction, null if bank account was used
        /// </summary>
        [JsonIgnore] public Card Card
        {
            get => card;
            set => card = value;
        }

        /// <summary>
        /// Payment flow
        /// MASTERPASS, VISA_CHECKOUT, ONE_CLICK_CARD, ONE_CLICK_CARD_RECURRING, CARD
        /// </summary>
        [JsonIgnore] public string PaymentFlow
        {
            get => paymentFlow;
            set => paymentFlow = value;
        }

        /// <summary>
        /// Payment Method (id)
        /// </summary>
        [JsonIgnore] public SimplifiedPayMethod PayMethod
        {
            get => payMethod;
            set => payMethod = value;
        }
    }
}