using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents request sent to retrieve transaction information
    /// </summary>
    public class TransactionRetrieveRequest : ValidatableObject
    {
        [Required] [JsonProperty] internal string orderId;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }
    }
}