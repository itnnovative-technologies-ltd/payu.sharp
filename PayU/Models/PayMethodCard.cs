using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Used for plain card integration with PayU
    /// REQUIRES CONTACT WITH TECH SUPPORT!
    /// </summary>
    [Serializable]
    public class PayMethodCard : ValidatableObject
    {
        [Required] [JsonProperty] private string number;
        [Required] [JsonProperty] private string expirationMonth;
        [Required] [JsonProperty] private string expirationYear;
        [Required] [JsonProperty] private string cvv;

        /// <summary>
        /// Credit card number
        /// </summary>
        public string Number
        {
            get => number;
            set => number = value;
        }

        /// <summary>
        /// Three (or more) digit code from back of the card
        /// </summary>
        public string Cvv
        {
            get => cvv;
            set => cvv = value;
        }

        public string ExpirationYear
        {
            get => expirationYear;
            set => expirationYear = value;
        }

        public string ExpirationMonth
        {
            get => expirationMonth;
            set => expirationMonth = value;
        }
    }
}