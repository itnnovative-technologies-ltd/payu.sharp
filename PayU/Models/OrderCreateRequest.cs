using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents order creation request
    /// </summary>
    [System.Serializable]
    public class OrderCreateRequest : ValidatableObject
    {
        [JsonProperty] internal string extOrderId;
        [JsonProperty] internal string notifyUrl;
        [Required] [JsonProperty] internal string customerIp;
        [Required] [JsonProperty] internal string merchantPosId = "0";
        [JsonProperty] internal string validityTime;
        [Required] [JsonProperty] internal string description;
        [JsonProperty] internal string additionalDescription;
        [Required] [JsonProperty] internal string currencyCode;
        [Required] [JsonProperty] internal string totalAmount;
        [JsonProperty] internal string cardOnFile;
        [JsonProperty] internal string continueUrl;
        [Recommended] [JsonProperty] internal Buyer buyer;
        [Required] [JsonProperty] internal List<Product> products;
        [JsonProperty] internal PayMethod payMethods;
        [JsonProperty] internal string recurring;

        /// <summary>
        /// External Order ID in your shop. MUST BE UNIQUE!
        /// </summary>
        [JsonIgnore] public string ExtOrderId
        {
            get => extOrderId;
            set => extOrderId = value;
        }

        /// <summary>
        /// URI address to which PayU will send notifications
        /// </summary>
        [JsonIgnore] public string NotifyUrl
        {
            get => notifyUrl;
            set => notifyUrl = value;
        }

        /// <summary>
        /// Customer IP Address
        /// </summary>
        [JsonIgnore] public string CustomerIp
        {
            get => customerIp;
            set => customerIp = value;
        }

        /// <summary>
        /// Your Merchant POS ID, obtained from PayU (usually known as Shop ID)
        /// </summary>
        [JsonIgnore] public int MerchantPosId
        {
            get => int.Parse(merchantPosId);
            set => merchantPosId = value.ToString();
        }

        /// <summary>
        /// Time of validity of your order.
        /// </summary>
        [JsonIgnore] public int ValidityTime
        {
            get => int.Parse(validityTime);
            set => validityTime = value.ToString();
        }

        /// <summary>
        /// Description of your transaction
        /// </summary>
        [JsonIgnore] public string Description
        {
            get => description;
            set => description = value;
        }

        /// <summary>
        /// Additional description, useful in some cases
        /// </summary>
        [JsonIgnore] public string AdditionalDescription
        {
            get => additionalDescription;
            set => additionalDescription = value;
        }

        /// <summary>
        /// ISO 4217 Currency Symbol for Transaction
        /// <see cref="System.Globalization.RegionInfo"/>
        /// </summary>
        [JsonIgnore]  public string CurrencyCode
        {
            get => currencyCode;
            set => currencyCode = value;
        }

        /// <summary>
        /// Total transaction price in lowest monetary unit.
        /// Usually just multiply your value by 100.
        /// Ex. 9.99 USD should be used here as 999.
        /// </summary>
        [JsonIgnore]  public int TotalAmount
        {
            get => int.Parse(totalAmount);
            set => totalAmount = value.ToString();
        }

        /// <summary>
        /// Informs about payment initiation side - merchant or client.
        /// Client - client starts manual payment
        /// Merchant - usually related to automated payments
        /// </summary>
        [JsonIgnore] public CardOnFile CardOnFile
        {
            get
            {
                switch (cardOnFile)
                {
                    case CardOnFileC.STANDARD_CARDHOLDER:
                        return CardOnFile.STANDARD_CARDHOLDER;
                    case CardOnFileC.STANDARD_MERCHANT:
                        return CardOnFile.STANDARD_MERCHANT;
                }

                return CardOnFile.STANDARD_CARDHOLDER;
            }
            set
            {
                switch (value)
                {
                    case CardOnFile.STANDARD_CARDHOLDER:
                        cardOnFile = CardOnFileC.STANDARD_CARDHOLDER;
                        break;
                    case CardOnFile.STANDARD_MERCHANT:
                        cardOnFile = CardOnFileC.STANDARD_MERCHANT;
                        break;
                }
            }
        }

        /// <summary>
        /// To this address client will be redirected after transaction.
        /// If transaction was failed, then ?error=501 will be appended to URI.
        /// THIS IS NOT RELIABLE WAY OF CHECKING PAYMENT STATUS.
        /// </summary>
        [JsonIgnore] public string ContinueUrl
        {
            get => continueUrl;
            set => continueUrl = value;
        }

        /// <summary>
        /// Represents person/company who is buying your products/services
        /// </summary>
        [JsonIgnore] public Buyer Buyer
        {
            get => buyer;
            set => buyer = value;
        }

        /// <summary>
        /// Represents list of products in order.
        /// </summary>
        [JsonIgnore] public List<Product> Products
        {
            get => products;
            set => products = value;
        }

        /// <summary>
        /// You can use it to select transaction method
        /// </summary>
        [JsonIgnore] public PayMethod PayMethod
        {
            get => payMethods;
            set => payMethods = value;
        }

        /// <summary>
        /// Sets recurring payment
        /// </summary>
        [JsonIgnore] public bool Recurring
        {
            get => recurring == "STANDARD";
            set => recurring = value ? "STANDARD" : "";
        }
    }
}