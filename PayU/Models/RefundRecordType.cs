using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents refund record data received with <see cref="RefundCreateResponse"/>
    /// </summary>
    public class RefundRecordType
    {
        [JsonProperty] internal string refundId;
        [JsonProperty] internal string extRefundId;
        [JsonProperty] internal string amount;
        [JsonProperty] internal string currencyCode;
        [JsonProperty] internal string description;
        [JsonProperty] internal string creationDateTime;
        [JsonProperty] internal string status;
        [JsonProperty] internal string statusDateTime;

        /// <summary>
        /// Refund ID in your shop
        /// </summary>
        [JsonIgnore] public string ExtRefundId
        {
            get => extRefundId;
            set => extRefundId = value;
        }

        /// <summary>
        /// PayU Refund ID
        /// </summary>
        [JsonIgnore] public string RefundId
        {
            get => refundId;
            set => refundId = value;
        }

        /// <summary>
        /// Refund payment in lowest monetary unit.
        /// $9.99 = 999
        /// </summary>
        [JsonIgnore] public int Amount
        {
            get => int.Parse(amount);
            set => amount = value.ToString();
        }

        /// <summary>
        /// ISO 4217 Currency Code
        /// </summary>
        [JsonIgnore] public string CurrencyCode
        {
            get => currencyCode;
            set => currencyCode = value;
        }

        /// <summary>
        /// Refund Description
        /// </summary>
        [JsonIgnore] public string Description
        {
            get => description;
            set => description = value;
        }

        /// <summary>
        /// DateTime of refund creation
        /// </summary>
        [JsonIgnore] public DateTime CreationDateTime
        {
            get => DateTime.Parse(creationDateTime);
            set => creationDateTime = value.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

        /// <summary>
        /// Refund status.
        /// Possible: PENDING, CANCELED, FINALIZED
        /// 
        /// </summary>
        [JsonIgnore] public string Status
        {
            get => status;
            set => status = value;
        }

        /// <summary>
        /// Date of last status change
        /// </summary>
        [JsonIgnore] public DateTime StatusDateTime
        {
            get => DateTime.Parse(statusDateTime);
            set => statusDateTime = value.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }
}