using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents order data received from server
    /// </summary>
    [Serializable]
    public class OrderRetrieveResponse
    {
        [JsonProperty] internal List<Order> orders;
        [JsonProperty] internal Status status;

        /// <summary>
        /// List of orders to be retrieved
        /// </summary>
        [JsonIgnore] public List<Order> Order
        {
            get => orders;
            set => orders = value;
        }

        /// <summary>
        /// Status of your request
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }
    }
}