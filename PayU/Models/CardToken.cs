using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents Card Token received as available payment method
    /// </summary>
    [Serializable]
    public class CardToken
    {
        [JsonProperty] internal string cardExpirationYear;
        [JsonProperty] internal string cardExpirationMonth;
        [JsonProperty] internal string cardNumberMasked;
        [JsonProperty] internal string cardScheme;
        [JsonProperty] internal string value;
        [JsonProperty] internal string brandImageUrl;
        [JsonProperty] internal bool preferred;
        [JsonProperty] internal string status;

        public int CardExpirationYear
        {
            get => int.Parse(cardExpirationYear);
            set => cardExpirationYear = value.ToString();
        }

        public string CardExpirationMonth
        {
            get => cardExpirationMonth;
            set => cardExpirationMonth = value;
        }

        /// <summary>
        /// Masked Card number ex.
        /// 411111******1111
        /// </summary>
        public string CardNumberMasked
        {
            get => cardNumberMasked;
            set => cardNumberMasked = value;
        }

        /// <summary>
        /// Card scheme ex. VS, MC
        /// </summary>
        public string CardScheme
        {
            get => cardScheme;
            set => cardScheme = value;
        }

        /// <summary>
        /// Token value, pass it to <see cref="PayMethod"/>
        /// </summary>
        public string Value
        {
            get => value;
            set => this.value = value;
        }

        /// <summary>
        /// Image of brand who issued the card
        /// </summary>
        public string BrandImageUrl
        {
            get => brandImageUrl;
            set => brandImageUrl = value;
        }

        /// <summary>
        /// Select this card as default when this is true
        /// </summary>
        public bool Preferred
        {
            get => preferred;
            set => preferred = value;
        }

        /// <summary>
        /// Checks if token/card is still valid
        /// </summary>
        public bool Status
        {
            get => status == "ACTIVE";
            set => status = value == true ? "ACTIVE" : "EXPIRED";
        }
    }
}