using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents bank account token
    /// </summary>
    public class PexToken
    {
        [JsonProperty] internal string accountNumber;
        [JsonProperty] internal string payType;
        [JsonProperty] internal string value;
        [JsonProperty] internal string name;
        [JsonProperty] internal string brandImageUrl;
        [JsonProperty] internal bool preferred;
        [JsonProperty] internal string status;

        /// <summary>
        /// Masked bank account number ex.
        /// 5311...7744
        /// </summary>
        public string AccountNumber
        {
            get => accountNumber;
            set => accountNumber = value;
        }

        /// <summary>
        /// PayType used in <see cref="PayMethod"/>
        /// </summary>
        public string PayType
        {
            get => payType;
            set => payType = value;
        }

        /// <summary>
        /// Token value to be used in <see cref="PayMethod"/>
        /// </summary>
        public string Value
        {
            get => value;
            set => this.value = value;
        }

        /// <summary>
        /// Token name
        /// </summary>
        public string Name
        {
            get => name;
            set => name = value;
        }

        /// <summary>
        /// Image of bank who issued this token
        /// </summary>
        public string BrandImageUrl
        {
            get => brandImageUrl;
            set => brandImageUrl = value;
        }

        /// <summary>
        /// Set this as default payment method when this is true
        /// </summary>
        public bool Preferred
        {
            get => preferred;
            set => preferred = value;
        }

        /// <summary>
        /// Checks if token is still valid
        /// </summary>
        public bool Status
        {
            get => status == "ACTIVE";
            set => status = value == true ? "ACTIVE" : "EXPIRED";
        }
    }
}