using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Payout Request Data
    /// </summary>
    public class PayoutRequestPayout
    {
        [JsonProperty] internal int amount;

        /// <summary>
        /// Represents money amount to be paid
        /// </summary>
        [JsonIgnore] public int Amount
        {
            get => amount;
            set => amount = value;
        }
    }
}