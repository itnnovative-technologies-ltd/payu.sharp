using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Responded by server on payout data request
    /// </summary>
    public class PayoutRetrieveResponse
    {
        [JsonProperty] internal Payout payout;
        [JsonProperty] internal Status status;

        /// <summary>
        /// Payout data
        /// </summary>
        [JsonIgnore] public Payout Payout
        {
            get => payout;
            set => payout = value;
        }

        /// <summary>
        /// Request status
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }
    }
}