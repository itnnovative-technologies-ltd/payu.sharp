using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents status of operation.
    /// You can use <see cref="StatusCodes"/> to make it easier
    /// </summary>
    public class Status
    {
        [JsonProperty] internal string statusCode;
        [JsonProperty] internal string statusDesc;
        
        /// <summary>
        /// Status Code
        /// </summary>
        [JsonIgnore] public int StatusCode
        {
            get => int.Parse(statusCode);
            set => statusCode = value.ToString();
        }

        /// <summary>
        /// Status Description
        /// </summary>
        [JsonIgnore] public string StatusDesc
        {
            get => statusDesc;
            set => statusDesc = value;
        }
    }
}