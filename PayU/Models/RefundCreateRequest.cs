using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents request to give a refund to client
    /// </summary>
    [Serializable]
    public class RefundCreateRequest : ValidatableObject
    {
        [Required] [JsonProperty] internal string orderId;
        [Required] [JsonProperty] internal RefundInfoType refund;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }

        /// <summary>
        /// Refund Information
        /// </summary>
        [JsonIgnore] public RefundInfoType Refund
        {
            get => refund;
            set => refund = value;
        }
    }
}