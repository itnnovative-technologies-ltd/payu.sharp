using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents Payment Method (used in <see cref="OrderCreateRequest"/>)
    /// </summary>
    public class PayMethod
    {
        [Required] [JsonProperty] internal string type;
        [JsonProperty] internal string value;
        [JsonProperty] internal string authorizationCode;
        [JsonProperty] internal PayMethodCard card;
        
        /// <summary>
        /// Payment Method Type
        /// Available: PBL, CARD_TOKEN, PAYMENT_WALL
        /// </summary>
        [JsonIgnore] public string Type
        {
            get => type;
            set => type = value;
        }

        /// <summary>
        /// Payment Symbol
        /// <see cref="http://developers.payu.com/en/overview.html#paymethods"/>
        /// </summary>
        [JsonIgnore] public string Value
        {
            get => value;
            set => this.value = value;
        }

        /// <summary>
        /// Code for "code" payments ex. BLIK
        /// </summary>
        [JsonIgnore] public string AuthorizationCode
        {
            get => authorizationCode;
            set => authorizationCode = value;
        }

        /// <summary>
        /// Used for plain card integration,
        /// REQUIRES CONTACT WITH PAYU SUPPORT
        /// </summary>
        public PayMethodCard Card
        {
            get => card;
            set => card = value;
        }
    }
}