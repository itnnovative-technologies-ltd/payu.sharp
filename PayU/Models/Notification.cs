using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents notification about order status change
    /// </summary>
    [Serializable]
    public class Notification
    {
        [JsonProperty] internal Order order;

        /// <summary>
        /// Order which changed status
        /// </summary>
        [JsonIgnore] public Order Order
        {
            get => order;
            set => order = value;
        }
    }
}