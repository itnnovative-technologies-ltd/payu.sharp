using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents payment method type used in <see cref="PayMethodsResponse"/>
    /// </summary>
    public class PayMethodType
    {
        [JsonProperty] internal string value;
        [JsonProperty] internal string name;
        [JsonProperty] internal string brandImageUrl;
        [JsonProperty] internal string status;
        [JsonProperty] internal int minAmount;
        [JsonProperty] internal int maxAmount;

        /// <summary>
        /// Type of payment
        /// </summary>
        [JsonIgnore] public string Value
        {
            get => value;
            set => this.value = value;
        }

        /// <summary>
        /// Payment Name
        /// </summary>
        [JsonIgnore] public string Name
        {
            get => name;
            set => name = value;
        }

        /// <summary>
        /// Payment Logo
        /// </summary>
        [JsonIgnore] public string BrandImageUrl
        {
            get => brandImageUrl;
            set => brandImageUrl = value;
        }

        /// <summary>
        /// Payment status
        /// ENABLED/DISABLED/TEMPORARY_DISABLED
        /// </summary>
        [JsonIgnore] public string Status
        {
            get => status;
            set => status = value;
        }

        /// <summary>
        /// Minimum amount to use payment method
        /// </summary>
        [JsonIgnore] public int MinAmount
        {
            get => minAmount;
            set => minAmount = value;
        }

        /// <summary>
        /// Max amount to use payment method
        /// </summary>
        [JsonIgnore] public int MaxAmount
        {
            get => maxAmount;
            set => maxAmount = value;
        }
    }
}