using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents bank account for transaction.
    /// BEWARE: Address might be concatenated into name
    /// </summary>
    [Serializable]
    public class BankAccount
    {
        [JsonProperty] internal string number;
        [JsonProperty] internal string name;
        [JsonProperty] internal string city;
        [JsonProperty] internal string postalCode;
        [JsonProperty] internal string street;
        [JsonProperty] internal string address;

        /// <summary>
        /// Bank account no. ex. IBAN
        /// </summary>
        [JsonIgnore] public string Number
        {
            get => number;
            set => number = value;
        }

        /// <summary>
        /// Bank account holder
        /// </summary>
        [JsonIgnore] public string Name
        {
            get => name;
            set => name = value;
        }

        [JsonIgnore] public string City
        {
            get => city;
            set => city = value;
        }

        [JsonIgnore] public string PostalCode
        {
            get => postalCode;
            set => postalCode = value;
        }

        [JsonIgnore] public string Street
        {
            get => street;
            set => street = value;
        }

        [JsonIgnore] public string Address
        {
            get => address;
            set => address = value;
        }
    }
}