using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents data received on order status update
    /// </summary>
    public class OrderStatusUpdateResponse
    {
        [JsonProperty] internal Status status;

        /// <summary>
        /// Order Status Update Response Code
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }
    }
}