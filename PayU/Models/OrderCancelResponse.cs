using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents data received on order cancellation
    /// </summary>
    [Serializable]
    public class OrderCancelResponse
    {
        [JsonProperty] internal string orderId;
        [JsonProperty] internal string extOrderId;
        [JsonProperty] internal Status status;

        /// <summary>
        /// PayU order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }

        /// <summary>
        /// Order ID in your shop
        /// </summary>
        [JsonIgnore] public string ExtOrderId
        {
            get => extOrderId;
            set => extOrderId = value;
        }

        /// <summary>
        /// Status of cancellation
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }
    }
}