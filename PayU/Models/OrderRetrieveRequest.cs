using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents order information download request
    /// </summary>
    [Serializable]
    public class OrderRetrieveRequest : ValidatableObject
    {
        [Required] [JsonProperty] internal string orderId;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }
    }
}