using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents Credit Card used to make payment
    /// </summary>
    [Serializable]
    public class Card
    {
        [JsonProperty] internal CardData cardData;

        /// <summary>
        /// Card Data
        /// </summary>
        [JsonIgnore] public CardData CardData
        {
            get => cardData;
            set => cardData = value;
        }
    }
}