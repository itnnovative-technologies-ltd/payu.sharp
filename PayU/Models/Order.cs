using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents Order with complete data
    /// </summary>
    [Serializable]
    public class Order
    {
        [JsonProperty] internal string orderId;
        [JsonProperty] internal string extOrderId;
        [JsonProperty] internal string orderCreateDate;
        [JsonProperty] internal string notifyUrl;
        [JsonProperty] internal string customerIp;
        [JsonProperty] internal string merchantPosId;
        [JsonProperty] internal string validityTime;
        [JsonProperty] internal string description;
        [JsonProperty] internal string additionalDescription;
        [JsonProperty] internal string currencyCode;
        [JsonProperty] internal string totalAmount;
        [JsonProperty] internal string status;
        [JsonProperty] internal Buyer buyer;
        [JsonProperty] internal List<Product> products;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }

        /// <summary>
        /// Your shop Order ID
        /// </summary>
        [JsonIgnore] public string ExtOrderId
        {
            get => extOrderId;
            set => extOrderId = value;
        }

        /// <summary>
        /// Date of order Creation
        /// </summary>
        [JsonIgnore] public DateTime OrderCreateDate
        {
            get => DateTime.Parse(orderCreateDate);
            set => orderCreateDate = value.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

        /// <summary>
        /// URL to send notifications (<see cref="Notification"/> to)
        /// </summary>
        [JsonIgnore] public string NotifyUrl
        {
            get => notifyUrl;
            set => notifyUrl = value;
        }

        /// <summary>
        /// Customer IP
        /// </summary>
        [JsonIgnore] public string CustomerIp
        {
            get => customerIp;
            set => customerIp = value;
        }

        /// <summary>
        /// POS ID of your PayU shop
        /// </summary>
        [JsonIgnore] public string MerchantPosId
        {
            get => merchantPosId;
            set => merchantPosId = value;
        }

        /// <summary>
        /// Time left to invalidate transaction (in seconds)
        /// </summary>
        [JsonIgnore] public int ValidityTime
        {
            get => int.Parse(validityTime);
            set => validityTime = value.ToString();
        }

        /// <summary>
        /// Description of your order
        /// </summary>
        [JsonIgnore] public string Description
        {
            get => description;
            set => description = value;
        }

        /// <summary>
        /// Additional description
        /// </summary>
        [JsonIgnore] public string AdditionalDescription
        {
            get => additionalDescription;
            set => additionalDescription = value;
        }

        /// <summary>
        /// ISO 4217 currency code
        /// </summary>
        [JsonIgnore] public string CurrencyCode
        {
            get => currencyCode;
            set => currencyCode = value;
        }

        /// <summary>
        /// Total price of order in lowest monetary unit.
        /// Ex. for USD 9.99 it will be 999.
        /// </summary>
        [JsonIgnore] public int TotalAmount
        {
            get => int.Parse(totalAmount);
            set => totalAmount = value.ToString();
        }

        /// <summary>
        /// Transaction status
        /// </summary>
        [JsonIgnore] public string Status
        {
            get => status;
            set => status = value;
        }

        /// <summary>
        /// Your client, who bought this order
        /// </summary>
        [JsonIgnore] public Buyer Buyer
        {
            get => buyer;
            set => buyer = value;
        }

        /// <summary>
        /// Things the client has bought
        /// </summary>
        [JsonIgnore] public List<Product> Products
        {
            get => products;
            set => products = value;
        }
    }
}