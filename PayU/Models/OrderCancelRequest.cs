using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents order cancellation request
    /// </summary>
    [Serializable]
    public class OrderCancelRequest : ValidatableObject
    {
        [Required] [JsonProperty] internal string orderId;

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }
    }
}