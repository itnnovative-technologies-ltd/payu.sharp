using System;
using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents data received on order creation
    /// </summary>
    [Serializable]
    public class OrderCreateResponse
    {
        [JsonProperty] internal string redirectUri;
        [JsonProperty] internal string orderId;
        [JsonProperty] internal string extOrderId;
        [JsonProperty] internal Status status;

        /// <summary>
        /// URI to redirect client to (transaction URI)
        /// </summary>
        [JsonIgnore]  public string RedirectUri
        {
            get => redirectUri;
            set => redirectUri = value;
        }

        /// <summary>
        /// Order ID in your shop
        /// </summary>
        [JsonIgnore] public string ExtOrderId
        {
            get => extOrderId;
            set => extOrderId = value;
        }

        /// <summary>
        /// PayU Order ID
        /// </summary>
        [JsonIgnore] public string OrderId
        {
            get => orderId;
            set => orderId = value;
        }

        /// <summary>
        /// Order creation status <see cref="StatusCodes"/> for more information.
        /// </summary>
        [JsonIgnore] public Status Status
        {
            get => status;
            set => status = value;
        }
    }
}