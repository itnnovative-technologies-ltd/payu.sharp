using System;
using System.Globalization;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents product you sell to buyer.
    /// </summary>
    [Serializable]
    public class Product : ValidatableObject
    {
        [Required] [JsonProperty] internal string name;
        [Required] [JsonProperty] internal string unitPrice;
        [Required] [JsonProperty] internal string quantity;
        [JsonProperty] internal bool @virtual;
        [JsonProperty] internal string listingDate;

        /// <summary>
        /// Product name
        /// </summary>
        [JsonIgnore] public string Name
        {
            get => name;
            set => name = value;
        }

        /// <summary>
        /// Product quantity
        /// </summary>
        public int Quantity
        {
            get => int.Parse(quantity);
            set => quantity = value.ToString();
        }

        
        
        /// <summary>
        /// Unit price of product.
        /// Given in smallest monetary unit.
        /// For example 9.99USD = 999
        /// </summary>
        [JsonIgnore] public int UnitPrice
        {
            get => int.Parse(unitPrice);
            set => unitPrice = value.ToString();
        }

        /// <summary>
        /// Represents if goods are virtual or not
        /// </summary>
        [JsonIgnore] public bool Virtual
        {
            get => @virtual;
            set => @virtual = value;
        }

        /// <summary>
        /// Date of putting product in sale.
        /// </summary>
        [JsonIgnore] public DateTime ListingDate
        {
            get => DateTime.Parse(listingDate);
            set => listingDate = value.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

    }
}