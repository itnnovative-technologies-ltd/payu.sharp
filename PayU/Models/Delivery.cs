using System;
using Newtonsoft.Json;
using PayU.Internal;

namespace PayU.Models
{
    /// <summary>
    /// Represents delivery location of goods
    /// </summary>
    [Serializable]
    public class Delivery : ValidatableObject
    {
        [Required] [JsonProperty] internal string street;
        [JsonProperty] internal string postalBox;
        [Required] [JsonProperty] internal string postalCode;
        [Required] [JsonProperty] internal string city;
        [JsonProperty] internal string state;
        [Required] [JsonProperty] internal string countryCode;
        [JsonProperty] internal string name;
        [Required] [JsonProperty] internal string recipientName;
        [JsonProperty] internal string recipientEmail;
        [JsonProperty] internal string recipientPhone;
        
        [JsonIgnore] public string Street
        {
            get => street;
            set => street = value;
        }

        [JsonIgnore] public string PostalBox
        {
            get => postalBox;
            set => postalBox = value;
        }

        [JsonIgnore] public string PostalCode
        {
            get => postalCode;
            set => postalCode = value;
        }

        [JsonIgnore] public string City
        {
            get => city;
            set => city = value;
        }

        [JsonIgnore] public string State
        {
            get => state;
            set => state = value;
        }

        /// <summary>
        /// Country code in ISO-3166 format
        /// </summary>
        [JsonIgnore] public string CountryCode
        {
            get => countryCode;
            set => countryCode = value;
        }

        /// <summary>
        /// Address name
        /// </summary>
        [JsonIgnore] public string Name
        {
            get => name;
            set => name = value;
        }

        [JsonIgnore] public string RecipientEmail
        {
            get => recipientEmail;
            set => recipientEmail = value;
        }

        /// <summary>
        /// Name of recipient
        /// </summary>
        [JsonIgnore] public string RecipientName
        {
            get => recipientName;
            set => recipientName = value;
        }

        [JsonIgnore] public string RecipientPhone
        {
            get => recipientPhone;
            set => recipientPhone = value;
        }
    }
}