using Newtonsoft.Json;

namespace PayU.Models
{
    /// <summary>
    /// Represents payout made from shop
    /// </summary>
    public class Payout
    {
        [JsonProperty] internal string payoutId;
        [JsonProperty] internal string amount;
        [JsonProperty] internal string status;

        /// <summary>
        /// Payout Id assigned by PayU
        /// </summary>
        [JsonIgnore] public string PayoutId
        {
            get => payoutId;
            set => payoutId = value;
        }

        /// <summary>
        /// Payout Status
        /// </summary>
        [JsonIgnore] public string Status
        {
            get => status;
            set => status = value;
        }

        /// <summary>
        /// Amount of money paid out
        /// </summary>
        [JsonIgnore] public int Amount
        {
            get => int.Parse(amount);
            set => amount = value.ToString();
        }
    }
}