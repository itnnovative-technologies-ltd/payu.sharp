namespace PayU
{
    public static class StatusCodes
    {
        // General status codes
        public const int SUCCESS = 200;
        public const int WARNING_CONTINUE_REDIRECT = 302;
        public const int WARNING_CONTINUE_3DS = 302;
        public const int WARNING_CONTINUE_CVV = 302;
        public const int ERROR_SYNTAX = 400;
        public const int ERROR_VALUE_INVALID = 400;
        public const int ERROR_VALUE_MISSING = 400;
        public const int ERROR_ORDER_NOT_UNIQUE = 400;
        public const int UNAUTHORIZED = 401;
        public const int UNAUTHORIZED_REQUEST = 403;
        public const int DATA_NOT_FOUND = 404;
        public const int TIMEOUT = 408;
        public const int BUSINESS_ERROR = 500;
        public const int ERROR_INTERNAL = 500;
        public const int GENERAL_ERROR = 500;
        public const int WARNING = 500;
        public const int SERVICE_NOT_AVAILABLE = 500;
        
        
        // Refund status codes
        
        /// <summary>
        /// Refund section is missing
        /// </summary>
        public const int MISSING_REFUND_SECTION = 8300;
        
        /// <summary>
        /// Transaction has not ended yet
        /// </summary>
        public const int TRANS_NOT_ENDED = 9101;
        
        /// <summary>
        /// There's no balance to make refund
        /// </summary>
        public const int NO_BALANCE = 9102;
        
        /// <summary>
        /// Refund amount is too big
        /// </summary>
        public const int AMOUNT_TO_BIG = 9103;
        
        /// <summary>
        /// Refund amount is too small
        /// </summary>
        public const int AMOUNT_TO_SMALL = 9104;
        
        /// <summary>
        /// Shop does not support refunds
        /// </summary>
        public const int REFUND_DISABLED = 9105;
        
        /// <summary>
        /// Refunds are made too often
        /// </summary>
        public const int REFUND_TO_OFTEN = 9106;
        
        /// <summary>
        /// Refund already made
        /// </summary>
        public const int PAID = 9108;
        
        /// <summary>
        /// Unknown error appeared
        /// </summary>
        public const int UNKNOWN_ERROR = 9111;
        
        /// <summary>
        /// ExtOrderId parameters mismatch
        /// </summary>
        public const int REFUND_IDEMPOTENCY_MISMATCH = 9112;
        
        /// <summary>
        /// Shop has not been set up properly
        /// </summary>
        public const int TRANS_BILLING_ENTRIES_NOT_COMPLETED = 9113;

    }
}