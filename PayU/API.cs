using System.Threading.Tasks;
using PayU.Internal;
using PayU.Models;

namespace PayU
{
    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// API is an static class that grants access to all API functionality.
    /// Using APIClient is more recommended.
    /// </summary>
    public class API
    {
        private static APIClient _client = new APIClient();

        /// <summary>
        /// Used to pick between production and development servers of PayU
        /// </summary>
        public bool UseSandbox
        {
            get => _client.UseSandbox;
            set => _client.UseSandbox = value;
        }
        
        /// <summary>
        /// Authorizes in PayU system.
        /// </summary>
        /// <param name="clientId">Client ID to be used as authorization</param>
        /// <param name="clientSecret">Client Secret</param>
        /// <returns>Authorization response, which is also set as API current key.</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<OAuthResponse> Authorize(int clientId, string clientSecret)
        {
            return await _client.Authorize(clientId, clientSecret);
        }

        /// <summary>
        /// Creates refund
        /// </summary>
        /// <returns><see cref="RefundCreateResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<RefundCreateResponse> CreateRefund(RefundCreateRequest request)
        {
            return await _client.CreateRefund(request);
        }

        /// <summary>
        /// Creates order
        /// </summary>
        /// <returns><see cref="OrderCreateResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<OrderCreateResponse> CreateOrder(OrderCreateRequest request)
        {
            return await _client.CreateOrder(request);
        }

        /// <summary>
        /// Cancels order if possible
        /// </summary>
        /// <returns><see cref="OrderCancelResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<OrderCancelResponse> CancelOrder(OrderCancelRequest request)
        {
            return await _client.CancelOrder(request);
        }

        /// <summary>
        /// Updates order status
        /// </summary>
        /// <returns><see cref="OrderStatusUpdateResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<OrderStatusUpdateResponse> UpdateOrderStatus(OrderStatusUpdateRequest request)
        {
            return await _client.UpdateOrderStatus(request);
        }

        /// <summary>
        /// Gets Order Data
        /// </summary>
        /// <returns><see cref="OrderRetrieveResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<OrderRetrieveResponse> GetOrderData(OrderRetrieveRequest request)
        {
            return await _client.GetOrderData(request);
        }

        /// <summary>
        /// Gets Transaction Data
        /// </summary>
        /// <returns><see cref="TransactionRetrieveResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<TransactionRetrieveResponse> GetTransactionData(TransactionRetrieveRequest request)
        {
            return await _client.GetTransactionData(request);
        }

        /// <summary>
        /// Downloads available payment methods
        /// </summary>
        /// <returns><see cref="PayMethodsResponse"/> with server-returned data</returns>
        [Verified("Patryk Pastuszka")]
        public static async Task<PayMethodsResponse> GetPaymentMethods(string lang = "en")
        {
            return await _client.GetPaymentMethods(lang);
        }

        /// <summary>
        /// Deletes token if possible
        /// </summary>
        /// <returns>Status of operation, 204 for success</returns>
        public static async Task<int> DeleteToken(string tkn)
        {
            return await _client.DeleteToken(tkn);
        }

        /// <summary>
        /// Retrieves payout from server
        /// </summary>
        /// <returns><see cref="PayoutRetrieveResponse"/> with server-returned data</returns>
        public async Task<PayoutRetrieveResponse> RetrievePayout(PayoutRetrieveRequest request)
        {
            return await _client.RetrievePayout(request);
        }

        /// <summary>
        /// Creates new payout
        /// </summary>
        /// <returns><see cref="PayoutResponse"/> with server-returned data</returns>
        public async Task<PayoutResponse> CreatePayout(PayoutRequest request)
        {
            return await _client.CreatePayout(request);
        }

    }
}